import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CombustivelComponent } from './pages/combustivel.component';
import { AdicionarHistoricoCombustivelComponent } from './pages/adicionar-historico-combustivel/adicionar-historico-combustivel.component';
import { ListarHistoricoCombustivelComponent } from './pages/listar-historico-combustivel/listar-historico-combustivel.component';
import { OperacoesComponent } from './pages/operacoes/operacoes.component';
import { HistoricoCombustivelService } from '../../core/services/historico-combustivel.service'
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CombustivelRoutingModule } from './combustivel-routing.module';
import { ListarDadosFiltradosComponent } from './pages/listar-dados-filtrados/listar-dados-filtrados.component';
import { ListarDadosBandeirasComponent } from './pages/listar-dados-bandeiras/listar-dados-bandeiras.component';
import { MediaDePrecoPorMunicipioComponent } from './pages/media-de-preco-por-municipio/media-de-preco-por-municipio.component';
import { ExcluirHistoricoCombustivelComponent } from './pages/excluir-historico-combustivel/excluir-historico-combustivel.component';
import { EditarHistoricoComponent } from './pages/editar-historico/editar-historico.component';

@NgModule({
  imports: [
    CommonModule,
    CombustivelRoutingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CombustivelComponent,
    AdicionarHistoricoCombustivelComponent,
    ListarHistoricoCombustivelComponent,
    OperacoesComponent,
    ListarDadosFiltradosComponent,
    ListarDadosBandeirasComponent,
    MediaDePrecoPorMunicipioComponent,
    ExcluirHistoricoCombustivelComponent,
    EditarHistoricoComponent,
  ],
  providers:[
    HistoricoCombustivelService
  ]
})
export class CombustivelModule { }
