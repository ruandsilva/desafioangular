import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdicionarHistoricoCombustivelComponent } from './pages/adicionar-historico-combustivel/adicionar-historico-combustivel.component';
import { CombustivelComponent } from './pages/combustivel.component';
import { ListarDadosFiltradosComponent } from './pages/listar-dados-filtrados/listar-dados-filtrados.component';
import { ListarHistoricoCombustivelComponent } from './pages/listar-historico-combustivel/listar-historico-combustivel.component';
import { OperacoesComponent } from './pages/operacoes/operacoes.component';
import { ListarDadosBandeirasComponent } from './pages/listar-dados-bandeiras/listar-dados-bandeiras.component';
import { MediaDePrecoPorMunicipioComponent } from './pages/media-de-preco-por-municipio/media-de-preco-por-municipio.component';
import { ExcluirHistoricoCombustivelComponent } from './pages/excluir-historico-combustivel/excluir-historico-combustivel.component';
import { EditarHistoricoComponent } from './pages/editar-historico/editar-historico.component';

const routes: Routes = [
  {
    path: '',
    component: CombustivelComponent,
    children: [
      {
        path: '',
        component: ListarHistoricoCombustivelComponent
      },
      {
        path: 'listar',
        component: ListarHistoricoCombustivelComponent
      },
      {
        path: 'adicionar',
        component: AdicionarHistoricoCombustivelComponent
      },
      {
        path: 'operacoes',
        component: OperacoesComponent
      },
      {
        path: 'listarPorDataColeta',
        component: ListarDadosFiltradosComponent
      },
      {
        path: 'listarPorDistribuidora',
        component: ListarDadosFiltradosComponent
      },
      {
        path: 'mediasPorBandeira',
        component: ListarDadosBandeirasComponent
      },
      {
        path: 'mediasPorMunicipio',
        component: ListarDadosBandeirasComponent
      },
      {
        path: 'mediaPrecoMunicipio',
        component: MediaDePrecoPorMunicipioComponent
      },
      {
        path: 'deletar/:id',
        component: ExcluirHistoricoCombustivelComponent
      },
      {
        path: 'editar/:id',
        component: EditarHistoricoComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CombustivelRoutingModule { }