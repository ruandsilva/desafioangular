import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HistoricoCombustivel } from 'src/app/core/models/historico-combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';
import { ValidatorsCustom } from 'src/app/utils/validators-custom';

@Component({
  selector: 'app-adicionar-historico-combustivel',
  templateUrl: './adicionar-historico-combustivel.component.html',
  styleUrls: ['./adicionar-historico-combustivel.component.scss']
})
export class AdicionarHistoricoCombustivelComponent extends FormBase implements OnInit, AfterViewInit {

  pipe = new DatePipe('en-US');

  constructor(
    private formBuilder: FormBuilder,
    private historicoCombustivelService: HistoricoCombustivelService,
    public router: Router
  ) { 
    super();
  }

  ngOnInit() {
    this.createFormGroup();
    this.validateMensageError();
  }

  createFormGroup() {
    this.form = this.formBuilder.group({
      combustivel:  ['', Validators.required],
      data:         ['', Validators.required],
      preco:        [0, [Validators.required,ValidatorsCustom.lessThanOne]],
    });
  }

  validateMensageError() {
    this.createValidateFieldMessage({
      combustivel: {
        required: 'Campo Combustivel é obrigatório.',
      },
      data: {
        required: 'Campo Data é obrigatório.',
      },
      preco: {
        required: 'Campo Preço é obrigatório.',
        lessThanOne: 'Preço deve ser maior que zero.'
      },
    });
  }

  onSubmit() {

    if(this.form.valid){
      const historico = new HistoricoCombustivel(this.form.value);
      historico.data = this.pipe.transform(historico.data, 'MM/dd/yyyy');

      this.historicoCombustivelService.cadastrarHistorico(historico).subscribe(
        
        response => {
          SweetalertCustom.showAlertTimer('Operação realizada com sucesso.', {type: 'success'}).then(
            result => {
              if (result.dismiss) {
                this.router.navigate(['combustivel/listar']);
              }
            }
          );
        },
        erro => {
          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });        
        }
      );
    }

  } 

}
