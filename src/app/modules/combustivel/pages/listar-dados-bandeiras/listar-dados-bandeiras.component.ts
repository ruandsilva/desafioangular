import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Combustivel } from 'src/app/core/models/combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';
import { Bandeira } from '../../../../core/models/bandeira.model'

@Component({
  selector: 'app-listar-dados-bandeiras',
  templateUrl: './listar-dados-bandeiras.component.html',
  styleUrls: ['./listar-dados-bandeiras.component.scss']
})
export class ListarDadosBandeirasComponent implements OnInit  {

  listaBandeiras: Bandeira[];

  nameScreen = '';

  constructor(
    private historicoCombustivelService: HistoricoCombustivelService,
    public router: Router
  ) { 
  }

  ngOnInit() {
    this.getNameScreen();
    this.operacao();
  }

  private getNameScreen() {
    if (this.router.url.includes('mediasPorBandeira')) {
      this.nameScreen = 'mediasPorBandeira';
    } else if (this.router.url.includes('mediasPorMunicipio')) {
      this.nameScreen = 'mediasPorMunicipio';
    }
  }

  operacao(){
    if(this.nameScreen === 'mediasPorBandeira'){
      this.valorMedioCompraVendaPorBandeira();
    }else if(this.nameScreen === 'mediasPorMunicipio'){
      this.valorMedioCompraVendaPorMunicipio();
    }
  }

  private valorMedioCompraVendaPorBandeira(){
    this.historicoCombustivelService.mediaDeCompraVendaPorBandeira().subscribe(
      response => {
        this.listaBandeiras = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

  private valorMedioCompraVendaPorMunicipio(){
    this.historicoCombustivelService.mediaDeCompraVendaPorMunicipio().subscribe(
      response => {
        this.listaBandeiras = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

}
