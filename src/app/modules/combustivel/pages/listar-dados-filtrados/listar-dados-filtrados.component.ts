import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Combustivel } from 'src/app/core/models/combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-listar-dados-filtrados',
  templateUrl: './listar-dados-filtrados.component.html',
  styleUrls: ['./listar-dados-filtrados.component.scss']
})
export class ListarDadosFiltradosComponent extends FormBase implements OnInit, AfterViewInit {

  listaCombustiveis: Combustivel[];
  municipio: string;

  nameScreen = '';

  constructor(
    private formBuilder: FormBuilder,
    private historicoCombustivelService: HistoricoCombustivelService,
    public router: Router) {
      super();
   }

  ngOnInit() {
    this.getNameScreen();
    this.operacao();
    this.createFormGroup();
    this.validateMensageError();
  }

  createFormGroup() {
    this.form = this.formBuilder.group({
      siglaRegiao:      ['', Validators.required],
    });
  }

  validateMensageError() {
    this.createValidateFieldMessage({
      siglaRegiao: {
        required: 'Campo da Sigla da Região é obrigatório para consulta.',
      },
    });
  }

  private getNameScreen() {
    if (this.router.url.includes('DataColeta')) {
      this.nameScreen = 'DataColeta';
    } else if (this.router.url.includes('Distribuidora')) {
      this.nameScreen = 'Distribuidora';
    }
  }

  operacao(){
    if(this.nameScreen === 'DataColeta'){
      this.listarCombustiveisPorDataColeta();
    }else if(this.nameScreen === 'Distribuidora'){
      this.listarCombustiveisPorDistribuidora();
    }
  }

  private listarCombustiveisPorDataColeta(){
    this.historicoCombustivelService.listarCombustivelAgrupadosPorDataColeta().subscribe(
      response => {
        this.listaCombustiveis = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

  private listarCombustiveisPorDistribuidora(){
    this.historicoCombustivelService.listarCombustivelAgrupadosPorDistribuidora().subscribe(
      response => {
        this.listaCombustiveis = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

  onSubmit(){

    if(this.form.valid){
      const combustivel = new Combustivel(this.form.value);

      this.historicoCombustivelService.listarCombustivelPorSiglaRegiao(combustivel.siglaRegiao).subscribe(

        response => {
          this.listaCombustiveis = response.body;
        },
        erro =>{
          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
        }
      );
    }
  }

  limparSubmit(){

    if(this.form.valid){
      const combustivel = new Combustivel(this.form.value);

      this.historicoCombustivelService.listarCombustivelAgrupadosPorDataColeta().subscribe(
        response => {
          this.listaCombustiveis = response.body
        },
        erro =>{
          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
        }
      );
    }
  }

}
