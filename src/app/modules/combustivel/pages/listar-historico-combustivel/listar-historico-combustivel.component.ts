import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HistoricoCombustivel } from 'src/app/core/models/historico-combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-listar-historico-combustivel',
  templateUrl: './listar-historico-combustivel.component.html',
  styleUrls: ['./listar-historico-combustivel.component.scss'],
  preserveWhitespaces: true
})
export class ListarHistoricoCombustivelComponent implements OnInit {

  listaHistorico: HistoricoCombustivel[];
  combustivel: string;

  constructor(
    private historicoCombustivelService: HistoricoCombustivelService,
    public router: Router
  ) { }

  ngOnInit() {
    this.listarHistoricos();
  }

  listarHistoricos(){
    this.historicoCombustivelService.listarHistoricos().subscribe(
      response => {
        this.listaHistorico = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

  search(){
    if(this.combustivel != ""){
      this.listaHistorico = this.listaHistorico.filter(res=>{
        return res.combustivel.toLocaleLowerCase().match(this.combustivel.toLocaleLowerCase());
      })  
    }else if(this.combustivel == ""){
      this.ngOnInit();
    }
  }
}
