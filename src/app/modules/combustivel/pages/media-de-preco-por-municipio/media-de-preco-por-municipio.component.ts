import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Combustivel } from 'src/app/core/models/combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-media-de-preco-por-municipio',
  templateUrl: './media-de-preco-por-municipio.component.html',
  styleUrls: ['./media-de-preco-por-municipio.component.scss']
})
export class MediaDePrecoPorMunicipioComponent  extends FormBase implements OnInit, AfterViewInit {

  precoMedio: number;

  constructor(
    private formBuilder: FormBuilder,
    private historicoCombustivelService: HistoricoCombustivelService,
  ) {
    super();
  }

  ngOnInit() {
    this.createFormGroup();
    this.validateMensageError();
  }

  createFormGroup() {
    this.form = this.formBuilder.group({
      municipio:      ['', Validators.required],
    });
  }

  validateMensageError() {
    this.createValidateFieldMessage({
      municipio: {
        required: 'Campo Municipio é obrigatório.',
      },
    });
  }

  onSubmit(){

    if(this.form.valid){
      const combustivel = new Combustivel(this.form.value);

      this.historicoCombustivelService.mediaPrecoPorMunicipio(combustivel.municipio).subscribe(

        response => {
          this.precoMedio = response.body
          SweetalertCustom.showAlertConfirm(`O Preço médio para o Municipio ${combustivel.municipio} é\n R$ ${this.precoMedio}`, { type: 'success' });
        },
        erro =>{

          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });

        }

      );
    }

  }
}
