/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MediaDePrecoPorMunicipioComponent } from './media-de-preco-por-municipio.component';

describe('MediaDePrecoPorMunicipioComponent', () => {
  let component: MediaDePrecoPorMunicipioComponent;
  let fixture: ComponentFixture<MediaDePrecoPorMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaDePrecoPorMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaDePrecoPorMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
