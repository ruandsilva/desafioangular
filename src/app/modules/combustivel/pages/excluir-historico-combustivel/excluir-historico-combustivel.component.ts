import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoricoCombustivel } from 'src/app/core/models/historico-combustivel.model';
import { HistoricoCombustivelService } from 'src/app/core/services/historico-combustivel.service';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-excluir-historico-combustivel',
  templateUrl: './excluir-historico-combustivel.component.html',
  styleUrls: ['./excluir-historico-combustivel.component.scss']
})
export class ExcluirHistoricoCombustivelComponent implements OnInit {

  historico: HistoricoCombustivel;
  id: number;

  constructor(
    private historicoCombustivelService: HistoricoCombustivelService,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.historicoCombustivelService.buscarHistoricoById(this.id).subscribe(
      response => {
      this.historico = response.body;
    });
  }



  confirmarDelete(){
    this.historicoCombustivelService.deletarHistorico(this.historico.id).subscribe(
      response => {
      }
    );
    SweetalertCustom.showAlertTimer('Operação realizada com sucesso.', {type: 'success'}).then(
      result => {
        if (result.dismiss) {
          this.router.navigate(['combustivel/listar']);
        }
      }
    );

  }

}
