import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarUsuarioComponent } from './pages/editar-usuario/editar-usuario.component';
import { ExcluirUsuarioComponent } from './pages/excluir-usuario/excluir-usuario.component';
import { InserirUsuarioComponent } from './pages/inserir-usuario/inserir-usuario.component';
import { ListarUsuariosComponent } from './pages/listar-usuarios/listar-usuarios.component';
import { UsuarioComponent } from './pages/usuario.component';

const routes: Routes = [
  {
    path: '',
    component: UsuarioComponent,
    children: [
      {
        path: '',
        component: ListarUsuariosComponent
      },
      {
        path: 'listar',
        component: ListarUsuariosComponent
      },
      {
        path: 'adicionar',
        component: InserirUsuarioComponent
      },
      {
        path: 'deletar/:id',
        component: ExcluirUsuarioComponent
      },
      {
        path: 'editar/:id',
        component: EditarUsuarioComponent
      },
      
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }