import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioComponent } from './pages/usuario.component';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListarUsuariosComponent } from './pages/listar-usuarios/listar-usuarios.component';
import { InserirUsuarioComponent } from './pages/inserir-usuario/inserir-usuario.component';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { ExcluirUsuarioComponent } from './pages/excluir-usuario/excluir-usuario.component';
import { EditarUsuarioComponent } from './pages/editar-usuario/editar-usuario.component';

@NgModule({
  declarations: [
    UsuarioComponent,
    ListarUsuariosComponent,
    InserirUsuarioComponent,
    ExcluirUsuarioComponent,
    EditarUsuarioComponent,
  ],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    UsuarioService,
  ]
})
export class UsuarioModule { }
