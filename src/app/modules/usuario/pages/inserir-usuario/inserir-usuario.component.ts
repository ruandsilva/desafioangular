import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/usuario.model';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-inserir-usuario',
  templateUrl: './inserir-usuario.component.html',
  styleUrls: ['./inserir-usuario.component.scss']
})
export class InserirUsuarioComponent extends FormBase implements OnInit, AfterViewInit {

  constructor(
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    public router: Router
  ) {
    super();
   }

  ngOnInit() {
    this.createFormGroup();
    this.validateMensageError();
  }

  createFormGroup() {
    this.form = this.formBuilder.group({
      nome:    ['', Validators.required],
      email:   ['', Validators.required],
      login:   ['', Validators.required],
      senha:   ['', Validators.required],
    });
  }

  validateMensageError() {
    this.createValidateFieldMessage({
      nome: {
        required: 'Campo Nome é obrigatório.',
      },
      email: {
        required: 'Campo Email é obrigatório.',
      },
      login: {
        required: 'Campo Login é obrigatório.',
      },
      senha: {
        required: 'Campo Senha é obrigatório.',
      },
    });
  }

  onSubmit() {

    if(this.form.valid){
      const usuario = new Usuario(this.form.value);

      this.usuarioService.inserirUsuario(usuario).subscribe(
        
        response => {
          SweetalertCustom.showAlertTimer('Operação realizada com sucesso.', {type: 'success'}).then(
            result => {
              if (result.dismiss) {
                this.router.navigate(['usuario/listar']);
              }
            }
          );
        },
        erro => {
          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });        
        }

      );
    }

  }

}
