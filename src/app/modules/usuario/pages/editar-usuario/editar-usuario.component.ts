import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/usuario.model';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { FormBase } from 'src/app/utils/form-base';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss']
})
export class EditarUsuarioComponent extends FormBase implements OnInit, AfterViewInit {

  usuario: Usuario;
  id: number;

  constructor(
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    public router: Router,
    private route: ActivatedRoute
  ) { 
    super();
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.usuarioService.buscarUsuarioById(this.id).subscribe(
      response => {
      this.usuario = response.body;
    });
    this.createFormGroup();
    this.validateMensageError();
  }

  createFormGroup() {
    this.form = this.formBuilder.group({
      nome:    ['', Validators.required],
      email:   ['', Validators.required],
      login:   ['', Validators.required],
      senha:   ['', Validators.required],
    });
  }

  validateMensageError() {
    this.createValidateFieldMessage({
      nome: {
        required: 'Campo Nome é obrigatório.',
      },
      email: {
        required: 'Campo Email é obrigatório.',
      },
      login: {
        required: 'Campo Login é obrigatório.',
      },
      senha: {
        required: 'Campo Senha é obrigatório.',
      },
    });
  }

  onSubmit() {

    
      const usuario = new Usuario(this.form.value);
      usuario.id = this.id;
      usuario.admin = true;
      this.usuarioService.atualizarUsuario(usuario).subscribe(
        
        response => {
          SweetalertCustom.showAlertTimer('Usuario Editado com sucesso.', {type: 'success'}).then(
            result => {
              if (result.dismiss) {
                this.router.navigate(['usuario/listar']);
              }
            }
          );
        },
        erro => {
          SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });        
        }

      );
    }

  

}
