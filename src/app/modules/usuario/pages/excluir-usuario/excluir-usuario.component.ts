import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/usuario.model';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-excluir-usuario',
  templateUrl: './excluir-usuario.component.html',
  styleUrls: ['./excluir-usuario.component.scss']
})
export class ExcluirUsuarioComponent implements OnInit {

  usuario: Usuario;
  id: number;

  constructor(
    private usuarioService: UsuarioService,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.usuarioService.buscarUsuarioById(this.id).subscribe(
      response => {
      this.usuario = response.body;
    });
  }



  confirmarDelete(){
    this.usuarioService.deletarUsuario(this.usuario.id).subscribe(
      response => {
      }
    );
    SweetalertCustom.showAlertTimer('Usuário deletado com sucesso.', {type: 'success'}).then(
      result => {
        if (result.dismiss) {
          this.router.navigate(['usuario/listar']);
        }
      }
    );

  }

}
