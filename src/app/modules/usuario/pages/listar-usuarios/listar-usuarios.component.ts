import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/usuario.model';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { SweetalertCustom } from 'src/app/utils/sweetalert-custom';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.scss'],
  preserveWhitespaces: true
})
export class ListarUsuariosComponent implements OnInit {

  usuarios: Usuario[];
  nome: string;

  constructor(
    private usuarioService: UsuarioService,
    public router: Router
  ) { }

  ngOnInit() {
    this.listarUsuarios();
  }

  listarUsuarios(){

    this.usuarioService.listarUsuarios().subscribe(
      response => {
        this.usuarios = response.body
      },
      erro =>{
        SweetalertCustom.showAlertConfirm('Falha na operação.', { type: 'error' });
      }
    );
  }

  search(){
    if(this.nome != ""){
      this.usuarios = this.usuarios.filter(res=>{
        return res.nome.toLocaleLowerCase().match(this.nome.toLocaleLowerCase());
      })  
    }else if(this.nome == ""){
      this.ngOnInit();
    }
  }
  
}
