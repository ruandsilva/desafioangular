import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuarioModule } from './modules/usuario/usuario.module';
import { CombustivelModule } from './modules/combustivel/combustivel.module';
import { PaginaNaoEncontradaComponent } from './layouts/pages/pagina-nao-encontrada/pagina-nao-encontrada.component';


const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/usuario/usuario.module#UsuarioModule'
  },
  {
    path: 'usuario',
    loadChildren: './modules/usuario/usuario.module#UsuarioModule'
  },
  {
    path: 'combustivel',
    loadChildren: './modules/combustivel/combustivel.module#CombustivelModule'
  },
  {
    path: '**',
    component: PaginaNaoEncontradaComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
