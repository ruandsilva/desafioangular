import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.model';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private controller = 'usuarios';

  constructor(private apiService: ApiService) { }

  listarUsuarios(){
    return this.apiService.get(`${this.controller}`);
  }

  inserirUsuario(obj: Usuario){
    return this.apiService.post(`${this.controller}`,obj);
  }

  deletarUsuario(id: number){
    return this.apiService.delete(`${this.controller}/${id}`);
  }

  buscarUsuarioById(id: number){
    return this.apiService.get(`${this.controller}/${id}`);
  }

  atualizarUsuario(obj: Usuario){
    return this.apiService.put(`${this.controller}`,obj);
  }
}
