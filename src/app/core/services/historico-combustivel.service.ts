import { Injectable } from '@angular/core';
import { HistoricoCombustivel } from '../models/historico-combustivel.model';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class HistoricoCombustivelService {

  private controllerHistorico = 'historico';
  private controllerCombustivel = 'combustivel';

  constructor(private apiService: ApiService) { }

  listarHistoricos(){
    return this.apiService.get(`${this.controllerHistorico}`);
  }

  cadastrarHistorico(obj: HistoricoCombustivel){
    return this.apiService.post(`${this.controllerHistorico}`,obj);
  }

  atualizarHistorico(obj: HistoricoCombustivel){
    return this.apiService.put(`${this.controllerHistorico}`,obj);
  }

  deletarHistorico(id: number){
    return this.apiService.delete(`${this.controllerHistorico}/${id}`);
  }

  buscarHistoricoById(id: number){
    return this.apiService.get(`${this.controllerHistorico}/${id}`);
  }

  listarCombustivelAgrupadosPorDataColeta(){
    return this.apiService.get(`${this.controllerCombustivel}/dados-agrupados-por-data-coleta`);
  }

  listarCombustivelAgrupadosPorDistribuidora(){
    return this.apiService.get(`${this.controllerCombustivel}/dados-agrupados-por-distribuidora`);
  }

  mediaDeCompraVendaPorBandeira(){
    return this.apiService.get(`${this.controllerCombustivel}/valor-media-compra-venda-bandeira`);
  }

  mediaDeCompraVendaPorMunicipio(){
    return this.apiService.get(`${this.controllerCombustivel}/valor-media-compra-venda-municipio`);
  }

  listarCombustivelPorSiglaRegiao(siglaRegiao: string){
    return this.apiService.get(`${this.controllerCombustivel}/dados-por-sigla/${siglaRegiao}`);
  }

  mediaPrecoPorMunicipio(municipio: string){
    return this.apiService.get(`${this.controllerCombustivel}/media-de-preco/${municipio}`);
  }
  

}