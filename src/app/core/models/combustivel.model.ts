export class Combustivel {

    id?: number;
    bandeira: string;
    codInstalacao: number;
    dataColeta: Date;
    municipio: string;
    produto: string;
    revendedora: string;
    siglaEstado: string;
    siglaRegiao: string;
    unidadeMedida: string;
    valorCompra: number;
    valorVenda: number;

    constructor(param: any) {
        this.id = param.id;
        this.bandeira = param.bandeira;
        this.codInstalacao = param.codInstalacao;
        this.dataColeta = param.dataColeta;
        this.municipio = param.municipio
        this.produto = param.produto
        this.revendedora = param.revendedora
        this.siglaEstado = param.siglaEstado
        this.siglaRegiao = param.siglaRegiao
        this.unidadeMedida = param.unidadeMedida
        this.valorCompra = param.valorCompra
        this.valorVenda = param.valorVenda
    }
}