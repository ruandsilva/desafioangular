export class HistoricoCombustivel {

    id?: number;
    combustivel: string;
    data: string;
    preco: number;

    constructor(param: any) {
        this.id = param.id;
        this.combustivel = param.combustivel;
        this.data = param.data;
        this.preco = param.preco;
    }
}