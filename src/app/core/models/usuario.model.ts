export class Usuario {

    id?: number;
    nome: string;
    email: string;
    login: string;
    admin: boolean;
    senha: string;

    constructor(param: any) {
        this.id = param.id;
        this.nome = param.nome;
        this.email = param.email;
        this.login = param.login;
        this.admin = param.admin;
        this.senha = param.senha;
    }
}