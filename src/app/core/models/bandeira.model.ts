export class Bandeira {

    atributo: string;
    mediaValorCompra: number;
    mediaValorVenda: number;

    constructor(param: any) {
        this.atributo = param.atributo;
        this.mediaValorCompra = param.mediaValorCompra;
        this.mediaValorVenda = param.mediaValorVenda;
    }
}